Cqlite
=============================
Yes, the name cqlite is both a rip off of the name sqlite but also literally
more correct when pronounced c-qual-ite. 

The existing standared Python Cassandra driver currently doesn't support
CQL v3.0 queries. This is the main reason I created this fork. 

Status as of 6/20/2013
----------------------------
- Implemented basic connection pool.
- Implemented a util class wrapper that endows instances with a lock.
- Started unit test for connection pool.

