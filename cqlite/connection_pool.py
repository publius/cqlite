
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from threading import Thread
from collections import deque
import time
import random

import cqlite
import cqlite.thrifteries
from cqlite.thrifteries import TTransport
from cqlite.thrifteries import ThriftConnection
from cqlite.native import NativeConnection

# `shared' is a callable that returns a parameterized 
# class. The new class binds an RLock to the `lock' 
# member attribute of new instances.
from cqlite.util import shared 

TTransportException = TTransport.TTransportException


__all__ = ['ConnectionPool']


## defaults
SERVERS = [('localhost', 9160)]
CREDENTIALS = {'user':None, 'password':None}


class ConnectionPool(object):
  '''
  A ConncetionPool manages a queue of connections to Cassandra.  Instead 
  of manually creating connections oneself, a pool instance should be used 
  to lease and release a limited number of connections. 
  '''

  class EvictionThread(Thread):
    '''
    This is an internal thread that periodically evicts idle 
    connections from a its connection pool. It searches the pool's 
    queue after an interval of 10 seconds by default. It continues 
    to evict connections until the queue has returned to the size 
    defined by its size attribute.
    '''

    def __init__(self, pool, interval=10):
      Thread.__init__(self)
      self.daemon = True
      self.pool = pool
      self.interval = interval

    def run(self):
      while True:
        time.sleep(self.interval)
        with self.pool.queue.lock:
          while len(self.pool.queue) > self.pool.max_queue_size:
            conn = self.pool.queue.popleft()
            if conn.open_socket:
              conn.close()
  

  def __init__(self, 
    keyspace, 
    servers=None, 
    credentials=None, 
    native=False,
    **kwargs
  ):
    '''
      Args:
        - keyspace: the string name of a keyspace.
        - servers: a list of (host:str, port:int) pairs.
        - credentials: a dict with "user" and "password" keys.

      Kwargs:
        - max_queue_size: max num connections in queue before eviction.
        - max_idle_size: max num of open connections in queue.
        - fill: true => pre-fill queue with max_idle_size connections.
    '''
    self.keyspace = keyspace
    self.servers = shared(deque)(servers if servers else SERVERS)
    self.credentials = credentials if credentials else CREDENTIALS
    self.max_queue_size = kwargs.get('max_queue_size', 25)
    self.max_idle_size = kwargs.get('max_idle_size', 5)
    self.cql_version = kwargs.get('cql_version', '3.0.0')
    self.queue = shared(deque)()
    
    if len(self.servers) > 1:
      random.shuffle(self.servers)

    if native:
      self.connection_class = NativeConnection 
    else:
      self.connection_class = ThriftConnection

    if kwargs.get('fill', True):
      self.fill()

    self.eviction_thread = ConnectionPool.EvictionThread(self)
    self.eviction_thread.start()

  def __del__(self):
    '''
      Close any remaining open connection in queue.
    '''
    with self.queue.lock:
      for conn in self.queue:
        if conn.open_socket:
          conn.close()

  def connect(self):
    '''
      Returns a new connection, using the parameters provided to the
      ConnectionPool constructor. This method does not mutate the queue.
      
      Returns: 
        - A new connection object.
    '''
    with self.servers.lock:
      if len(self.servers) > 1:
        host, port = self.servers.popleft()
        self.servers.append((host,port))
      else:
        host, port = self.servers[0]
    try:
      return self.connection_class(
        host, port,
        self.keyspace, 
        user=self.credentials.get('user'),
        password=self.credentials.get('password'),
        consistency_level='ONE',
        cql_version=self.cql_version,
        transport=None,
        pool=self,
      )
    except TTransportException as e:
      print e.message # TODO: log this
    
  def fill(self):
    '''
      Fills the pool wih as many open connection as it takes to reach
      the size limit defined by self.max_idle_size.
    '''
    with self.queue.lock:
      for i in range(self.max_idle_size - len(self.queue)):
        conn = self.connect()
        if conn is not None:
          self.queue.append(conn)

  def lease(self):
    '''
      If the queue is not empty, dequeue and return a connection.
      Otherwise, immediately Creates and returns a new connection.

      Returns: 
        - The next available connection object.
    '''
    with self.queue.lock:
      if not self.queue:
        return self.connect()
      else:
        return self.queue.popleft()

  def release(self, conn):
    '''
      Returns a leased connection instance to the queue, provided the 
      queue has not reached its maximum size. If the connection is already
      closed, it is immediately discarded.

      Args:
        - conn: A connection instance.
    '''
    with self.queue.lock:
      if len(self.queue) < self.max_queue_size:
        self.queue.append(conn)
      else:
        conn.close()
  
  def execute(self, query, data=None):
    '''
      Try to execute a query, interpolating dictionary data, returing a 
      cursor. This is just for convenience.
      
      Args:
        - query: A query string.
        - data: a dict of data to interpolate in query string.
    '''
    conn = self.lease()
    if conn is not None:
      cur = conn.cursor()
      if cur.execute(query, data):
        return cur
  
  def cursor(self):
    '''
      Return a cursor with a pooled connection.
    '''
    conn = self.lease()
    return conn.cursor()

