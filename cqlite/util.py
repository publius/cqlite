#!/usr/bin/env python
# module cqlite.util

from threading import RLock


class Singleton(type):
 
  _instances = {}

  def __call__(cls, *args, **kwargs):
    if cls not in cls._instances:
      cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
    return cls._instances[cls]


class CachedSharedClassWrapper(object):
  '''
    A singleton class. An instance of this class is a callable object that 
    takes a class as its first argument and returns a new parameterized 
    class whose constructor associates a Lock with each new instance.

    It caches the parameterized class to avoid redefining the same
    class each time the wrapper is applied.

    Usage example:
      >>> queue = shared(deque)([1, 2, 3])
      >>> with queue.lock:
      >>>   x = queue.popleft()  

  '''

  __metaclass__ = Singleton

  _cache = {}

  def __call__(self, cls, lock=RLock):
    '''
      Returns a parameterized class that binds a lock object to
      the `lock' instance attribute.

      Args:
        - cls: a class to wrap.
        - lock a threading.lock class or subclass.
    '''
    shared_cls = self._cache.get(cls.__name__)
    if not shared_cls:
      name = 'Shared' + cls.__name__
      def __init__(self, *args, **kwargs):
        cls.__init__(self, *args, **kwargs)
        self.lock = lock()
      shared_cls = type(
        name, (cls,), {'__init__':__init__}
      )
      self._cache[cls.__name__] = shared_cls
    return shared_cls


# exported public class wrapper callable
shared = CachedSharedClassWrapper()

